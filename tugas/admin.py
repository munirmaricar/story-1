from django.contrib import admin
from .models import (
    Friend,
    ClassYear,
    )

class FriendAdmin(admin.ModelAdmin):
    list_display = ('name', 'year',)

class ClassYearAdmin(admin.ModelAdmin):
    list_display = ('year',)

admin.site.register(Friend, FriendAdmin)
admin.site.register(ClassYear, ClassYearAdmin)
