from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.exceptions import ValidationError

class ClassYear(models.Model):
    # Year identity
    year = models.IntegerField(default=2018,
        validators=[
            MaxValueValidator(2019),
            MinValueValidator(1986)
        ])

    # Object Data
    created_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.year)

    class Meta:
        ordering = ('year',)
        verbose_name = 'year'
        verbose_name_plural = 'years'

class Friend(models.Model):
    # Friend Identity
    name = models.CharField(max_length=50, unique=True)
    year = models.ForeignKey(ClassYear, on_delete=models.CASCADE)
    hobby = models.CharField(max_length=30)
    favourite_food = models.CharField(max_length=30)
    favourite_beverage = models.CharField(max_length=30)

    # Object Data
    updated_on = models.DateTimeField(auto_now=True)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)
        verbose_name = 'friend'
        verbose_name_plural = 'friends'
