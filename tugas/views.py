from django.shortcuts import render, HttpResponse, get_object_or_404, redirect
from django.views import generic
from django.http import HttpResponseRedirect


def home(request):
    response = {}
    return render(request, 'index.html', response)

